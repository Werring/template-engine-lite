<?php
/**
 * Index, Template Engine Lite in action
 *
 * @author    Werring <info@t-werring.nl>
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @copyright 2013-2014 Werring webdevelopment
 * @package   Template
 */
require_once "Template.php";
require_once "Handler.php";


$handler = new Handler();
$get = $handler->getParameters();

switch($handler->getPageType()){
	case 'static':
		echo @file_get_contents("templates/".$get["static"]);
		break;
	case 'page':
		/**
		 * Invullen van het Template moet natuurlijk gedaan worden door code
		 * (uit database halen oid)
		 */
		$templateVars = array();
		$templateVars["title"] = "pagina titel";
		$templateVars["subtitle"] = "subtitel";
		$templateVars["main_content"] = "<p>
Aliquam a tincidunt justo. Nullam sed sapien tellus. Maecenas nec nulla vel justo molestie ultrices sit amet nec nibh. Morbi vulputate hendrerit nisi, et eleifend arcu. Sed et turpis lorem. Quisque congue et neque vel viverra. Nam elementum facilisis ultricies. Donec lobortis at tellus ac egestas. Donec laoreet, purus vitae vestibulum dapibus, justo odio adipiscing diam, a semper metus elit non turpis. Integer sagittis, orci id viverra facilisis, sem justo placerat enim, ut pellentesque nisl ipsum et dui. Nam sit amet nulla eros.
</p><p>
Fusce ac lectus a nisl pulvinar convallis non in velit. Etiam neque ligula, porttitor interdum laoreet sed, tempor et orci. Morbi aliquam felis in erat ullamcorper facilisis. Integer commodo orci leo, non auctor ante blandit et. Nulla eu ullamcorper dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc hendrerit dapibus urna, placerat aliquam velit suscipit quis. Phasellus porta odio et laoreet ultricies. Phasellus posuere erat at nibh pellentesque mollis. Fusce ut ultrices nulla. Morbi dolor ligula, aliquet eget vehicula nec, varius et nulla. Quisque posuere, diam sit amet ullamcorper adipiscing, metus ante porttitor augue, in semper elit ligula at libero. Curabitur lacus sapien, feugiat vitae enim sit amet, blandit mollis tellus. Morbi id aliquam nisl. In lobortis pharetra nulla, at scelerisque turpis rutrum luctus.
</p><p>
Mauris tortor neque, hendrerit et mauris scelerisque, interdum rutrum odio. Donec in arcu lacinia tellus suscipit mattis. Phasellus sed bibendum enim. Suspendisse pharetra turpis quis nisi ullamcorper sollicitudin. Vivamus rutrum elit dolor, in ultrices elit congue eu. Suspendisse facilisis purus nec venenatis viverra. Ut lorem ligula, fringilla eget tellus at, auctor scelerisque justo.</p>";
		$templateVars["more_sidebar"] = "Inloggen";
		$templateVars["more_sidebar_content"] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vel luctus ligula. Pellentesque id ultricies metus. Mauris volutpat lectus eget dictum facilisis. Nunc euismod pulvinar sodales. Nunc egestas velit magna, non commodo lacus aliquet eu. Pellentesque at erat velit. Donec congue leo id est convallis, in tempus turpis ornare. Sed vel erat id neque lacinia placerat. Nullam vitae nisl enim. Aliquam nec eros tempus purus cursus eleifend quis id tellus. Nunc congue fringilla erat, et elementum arcu. Morbi sed magna ut nulla lacinia rutrum. Etiam iaculis tortor magna, id dictum quam consectetur egestas.";
		$templateVars["sidebar"] = "Template";
		$templateVars["sidebar_content"] = "<a href='/templates/design_foundation.html'>Template</a>";
		$templateVars["footer"] = "&copy; 2013 T. Werring";
		$templateVars["heading"] = ucfirst($handler->getPageType()) ." &there4; " . ucfirst($get['page']);
		$templateVars["subtitle"] = "GET: " . var_export($get,true);
		
		$template = new Template("design_foundation.html");
		$template->setTemplateVars($templateVars);
		$template->render();
		$template->parse();

		break;
}






