#Template Engine Lite

Preview can be found on [this page](http://tegit.t-werring.nl/)

##Origin
The Template Engine Lite has been developed for 2nd year students of [Mediacollege Amsterdam](http://ma-web.nl/) (MBO4)
As first introduction to OOP, the separation of code from design and mod\_rewrite.

##Adding parameters SEO friendly
[/pagename/paramkey/paramvalue](http://tegit.t-werring.nl/pagename/paramkey/paramvalue)

