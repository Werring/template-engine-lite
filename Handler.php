<?php
/**
 * Template Engine Lite
 * 
 * URL Handler
 *
 * @author    Werring <info@t-werring.nl>
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @copyright 2013-2014 Werring webdevelopment
 * @package   Template
 */

class Handler {
	/**
	 * Default display type
	 * @const string
	 */
	const DEFAULT_TYPE = "page";
	/** 
	 * Requested Page
	 * @var string
	 */
	private $_URI;
	/**
	 * @var string
	 * @static
	 */
	private static $_pagetypes ="page|static";
	private $_parameters = array();
	private $_pagetype;

	/**
	 * Constructor sets default page to "home"
	 */
	public function __construct(){
		$this->_URI = (isset($_GET["p"]) ? $_GET["p"] : "home");
		$this->proccess();
	}

	/**
	 * Process SEO friendly URL
	 */
	private function proccess(){
		if(!preg_match('~^('.self::$_pagetypes.')/~',$this->_URI)){
			$this->_URI = self::DEFAULT_TYPE . "/" . $this->_URI;
		}
		$this->_pagetype = preg_replace('~^([^/]+)/.*$~',"$1",$this->_URI);
		parse_str(preg_replace("~([^/]+)/?([^/]+)?/?~","$1=$2&",$this->_URI),$this->_parameters);
	}

	/**
	 * Returns SEO friendly parameters
	 * @return array
	 */
	public function getParameters(){
		return $this->_parameters;
	}

	/**
	 * Returns requested page type
	 * @return mixed
	 */
	public function getPageType(){
		return $this->_pagetype;
	}
}
