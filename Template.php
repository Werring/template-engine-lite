<?php
/**
 * Template Engine Lite
 *
 * @author    Werring <info@t-werring.nl>
 * @license   http://opensource.org/licenses/gpl-license.php GNU Public License
 * @copyright 2013-2014 Werring webdevelopment
 * @package   Template
 */
class Template
{
	/**
	 * Template vars to be replaced
	 *
	 * @var array $_vars
	 */
	protected $_vars = array();
	/**
	 * HTML from template
	 *
	 * @var string $_html
	 */
	protected $_html;

	/**
	 * Constructor
	 *
	 * Loads templatefile or throws exception if file is not found
	 *
	 * @param $templateFile
	 *
	 * @throws ErrorException
	 */
	public function __construct($templateFile)
	{
		$templateFile = "templates/" . $templateFile;
		if (file_exists($templateFile)) $this->_html = file_get_contents($templateFile);
		else throw new ErrorException('Could not find template file: ' . $templateFile);
	}

	/**
	 * Renders all templatevars
	 */
	public function render()
	{
		foreach ($this->_vars as $key => $value) {
			$this->_html = str_replace('#{' . $key . '}', $value, $this->_html);
		}
	}

	/**
	 * Template parser
	 *
	 * Parse template or only return it if $returnOnly is true
	 *
	 * @param bool $returnOnly
	 *
	 * @return string
	 */
	public function parse($returnOnly = false)
	{
		if (!$returnOnly) echo $this->_html;

		return $this->_html;
	}

	/**
	 * Add template var $name with $value
	 *
	 * @param string $name
	 * @param string $value
	 */
	public function templateVar($name, $value)
	{
		$this->_vars[$name] = $value;
	}

	/**
	 * Add array to templatevars
	 *
	 * @param array $array Assoc array
	 */
	public function setTemplateVars($array)
	{
		$this->_vars = array_merge($this->_vars, $array);
	}
}


